#include <iostream>

class AbstractSort
{
	// Shell sort
public:
	// main algorithm (template method)
	void sort(int *_arr, int _size)
	{
		for (int g = _size / 2; g > 0; g /= 2)
			for (int i = g; i < _size; i++)
				for (int j = i - g; j >= 0; j -= g)
					if (needSwap(_arr[j], _arr[j + g]))
						std::swap(_arr[j], _arr[j + g]);
	}
private:
	virtual bool needSwap(int, int) = 0;
};

class SortUp : public AbstractSort
{
	bool needSwap(int a, int b) override
	{
		return (a > b);
	}
};
class SortDown : public AbstractSort
{
	/* virtual */
	bool needSwap(int a, int b) override
	{
		return (a < b);
	}
};

int main()
{
	int a[] = { 6, 11, 3, 6, 9, 12, 5, 7, 1, 5, 4 };
	for (int i{}; i < 11; i++)
		std::cout << a[i] << " ";
	std::cout << std::endl;

	AbstractSort * sort = new SortUp;
	sort->sort(a, 11);

	for (int i{}; i < 11; i++)
		std::cout << a[i] << " ";
	std::cout << std::endl;

	delete sort;
	sort = new SortDown;
	sort->sort(a, 11);

	for (int i{}; i < 11; i++)
		std::cout << a[i] << " ";
	std::cout << std::endl;

	delete sort;
}