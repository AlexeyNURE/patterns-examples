#include <iostream>

struct Transport
{
	Transport() = default;
	virtual void deliever() = 0;
};

struct Truck
	: public Transport
{
	Truck() = default;
	void deliever() override
	{
		std::cout << "Delievering on the ground by truck\n";
	}
};

struct Train
	: public Transport
{
	Train() = default;
	void deliever() override
	{
		std::cout << "Delievering on the ground by Train\n";
	}
};

struct Boat
	: public Transport
{
	Boat() = default;

	void deliever() override
	{
		std::cout << "Delievering on the water by boat\n";
	}
};

struct Creator
{
	/*
	creator usually does something, expept of creating objects
	for exhample, IT company creats programmers, but the main purpose is to write code.
	*/
	virtual void someOperation() 
	{}

	/*
	if there is a big variaty of transports, we can pass as an argument an info about what transport to create
	*/
	virtual Transport * CreateTransport() = 0;
};

struct TruckCreator
	: public Creator
{
	Transport * CreateTransport() override
	{
		return new Truck();
	}
};

struct TrainCreator
	: public Creator
{
	Transport * CreateTransport() override
	{
		return new Train();
	}
};

struct BoatCreator
	: public Creator
{
	Transport * CreateTransport() override
	{
		return new Boat();
	}
};

int main()
{
	Creator * Create = new BoatCreator();
	Transport * transport = Create->CreateTransport();
	transport->deliever();


	delete Create;
	delete transport;
	return 0;
}