#include <iostream>

struct Chair
{
	virtual void sitOn() {};
	virtual ~Chair() = default;
};

struct Sofa
{
	virtual void sitOn() = 0;
	virtual void LieDown() = 0;
	virtual ~Sofa() = default;
};

struct Table
{
	virtual void sitOn() = 0;
	virtual void drinkCoffee() = 0;
	virtual ~Table() = default;

};

struct VictorianChair
	: public Chair
{
	void sitOn() override
	{
		std::cout << "Sat down on victorian chair\n";
	}
};

struct ModernChair
	: public Chair
{
	void sitOn() override
	{
		std::cout << "Sat down on modern chair\n";
	}
};

struct VictorianSofa
	: public Sofa
{
	void sitOn() override
	{
		std::cout << "Sat down on victorian sofa\n";
	}

	void LieDown() override
	{
		std::cout << "Lain down on vicrorian sofa\n";
	}
};

struct ModernSofa
	: public Sofa
{
	void sitOn() override
	{
		std::cout << "Sat down on modern sofa\n";
	}

	void LieDown() override
	{
		std::cout << "Lain down on modern sofa\n";
	}
};

struct VictorianTable
	: public Table
{
	void sitOn() override
	{
		std::cout << "Sat down on victorian table\n";
	}

	void drinkCoffee() override
	{
		std::cout << "Drunk coffee on victorian table\n";
	}
};


struct ModernTable
	: public Table
{
	void sitOn() override
	{
		std::cout << "Sat down on modern table\n";
	}

	void drinkCoffee() override
	{
		std::cout << "Drunk coffee on modern table\n";
	}
};


struct FurnitureFactory
{
	virtual Chair * createChair() = 0;
	virtual Sofa * createSofa() = 0;
	virtual Table * createTable() = 0;

	virtual ~FurnitureFactory() = default;
};

struct VictorianFurnitureFactory
	: public FurnitureFactory
{
	Chair * createChair() override
	{
		return new VictorianChair();
	}
	Sofa * createSofa() override
	{
		return new VictorianSofa();
	}
	Table * createTable() override
	{
		return new VictorianTable();
	}

};

struct ModernFurnitureFactory
	: public FurnitureFactory
{
	Chair * createChair() override
	{
		return new ModernChair();
	}
	Sofa * createSofa() override
	{
		return new ModernSofa();
	}
	Table * createTable() override
	{
		return new ModernTable();
	}
};

struct Client
{
	FurnitureFactory * m_Factory;

	Client(FurnitureFactory * _factory)
	{
		m_Factory = _factory;
	}
	~Client()
	{
		delete m_Factory;
	}
	void doSome();
};

int main()
{
	Client Alexey(new ModernFurnitureFactory());
	auto Sofa = Alexey.m_Factory->createSofa();
	Sofa->LieDown();
	auto Table = Alexey.m_Factory->createTable();
	Table->drinkCoffee();

	delete Sofa;
	delete Table;
	return 0;
}