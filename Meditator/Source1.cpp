#include <iostream>
#include <cassert>
#include <stack>
#include <string>

class Editor
{
	std::string m_Text;
	int m_x;
	int m_y;
	int m_SelectionWidth;
public:
	Editor() = default;

	~Editor() = default;

	void setText(std::string const & _text)
	{
		m_Text = _text;
	}

	void SetCursor(int _x, int _y)
	{
		m_x = _x;
		m_y = _y;
	}

	void setSelectionWidth(int _width)
	{
		m_SelectionWidth = _width;
	}

	std::string const & getText() const
	{
		return m_Text;
	}

	std::pair<int, int> getCoords() const
	{
		return std::make_pair(m_x, m_y);
	}

	int getSelectionWidth() const
	{
		return m_SelectionWidth;
	}

	class Snapshot
	{
		friend class Editor;
		std::string m_Text;
		int m_x;
		int m_y;
		int m_SelectionWidth;
	public:
		Snapshot(std::string const & _str, int _x, int _y, int _width)
			:m_Text(_str), m_x(_x), m_y(_y), m_SelectionWidth(_width)
		{
		}

		~Snapshot() = default;
	};

	Snapshot * createSnapshot()
	{
		return new Snapshot(m_Text, m_x, m_y, m_SelectionWidth);
	}

	void restore(Snapshot * _shot)
	{
		m_SelectionWidth = _shot->m_SelectionWidth;
		m_Text = _shot->m_Text;
		m_x = _shot->m_x;
		m_y = _shot->m_y;
	}
};

class App
{
};

class Command
{
	std::stack<Editor::Snapshot*> m_History;
	void popLastShot()
	{
		delete m_History.top();
		m_History.pop();
	}
protected:
	Editor * m_Editor;
public:
	Command(Editor * _editor)
		: m_Editor(_editor)
	{}

	virtual ~Command()
	{
		while (m_History.size())
		{
			delete m_History.top();
			m_History.pop();
		}
	}

	void makeBackup()
	{
		m_History.push(m_Editor->createSnapshot());
	}

	void undo()
	{
		if (!m_History.empty())
		{
			m_Editor->restore(m_History.top());
			popLastShot();
		}
	}

	virtual void execute() = 0;
};

class CopyCommand : public Command
{
	int m_SelectionWidth;
public:
	CopyCommand(Editor * _editor, int _width)
		:Command(_editor), m_SelectionWidth(_width)
	{}

	void execute() override
	{
		m_Editor->setSelectionWidth(m_SelectionWidth);
	}
};

class PasteCommand : public Command
{
	std::string m_Text;
public:
	PasteCommand(Editor * _editor, std::string const & _str)
		:Command(_editor), m_Text(_str)
	{
	}

	void execute() override
	{
		makeBackup();
		m_Editor->setText(m_Text);
	}
};

class CutCommand : public Command
{
public:
	CutCommand(Editor * _editor)
		:Command(_editor)
	{
	}

	void execute() override
	{
		makeBackup();
		m_Editor->setText("");
	}
};

class CursorCommand : public Command
{
	int m_x, m_y;
public:
	CursorCommand(Editor * _editor, int _x, int _y)
		:Command(_editor), m_x(_x), m_y(_y)
	{
	}

	void execute() override
	{
		makeBackup();
		m_Editor->SetCursor(m_x, m_y);
	}
};

//struct App
//{
//	Editor * activeEditor;
//public:
//	App() = default;
//
//	void executeCommand(Command * _command)
//	{
//		_command->execute();
//	}
//
//	void undo()
//	{
//		activeEditor->restore()
//	}
//};

int main()
{
	// bad exhample, because every command has it's own snapshot stack(history)
	// need to have one history in app class

	Editor  * editor = new Editor;
	editor->setText("text one");
	editor->setSelectionWidth(3);
	editor->SetCursor(0, 0);
	PasteCommand paste(editor, "text");
	CursorCommand cur(editor, 5, 5);
	CopyCommand copy(editor, 10);

	paste.execute();
	assert(editor->getText() == "text");
	
	cur.execute();
	assert(editor->getCoords() == std::make_pair(5, 5));

	copy.execute();
	assert(editor->getSelectionWidth() == 10);

	copy.undo();

	assert(editor->getSelectionWidth() == 10);
	assert(editor->getCoords() == std::make_pair(5, 5));
	assert(editor->getText() == "text");

	cur.undo();

	assert(editor->getSelectionWidth() == 3);
	assert(editor->getCoords() == std::make_pair(0, 0));
	assert(editor->getText() == "text");

	paste.undo();

	assert(editor->getSelectionWidth() == 3);
	assert(editor->getCoords() == std::make_pair(0, 0));
	assert(editor->getText() == "text one");
	
	delete editor;

	
	return 0;
}