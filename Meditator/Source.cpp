#include <iostream>
#include <string>
#include <cassert>
class Mediator;

class Dialog // component
{
	Mediator * m_Mediator;
protected:
	std::string m_Name;
public:
	Dialog(Mediator * _mediator, std::string const & _name)
		: m_Mediator(_mediator), m_Name(_name)
	{
	}

	~Dialog() = default;

	virtual void changed();
	
	virtual void updateDialog() = 0;
	
	virtual void queryDialog() = 0;
};

class List : public Dialog
{
public:
	List(Mediator * _mediator, std::string const & _name)
		: Dialog(_mediator, _name)
	{
	}

	void updateDialog() override
	{
		std::cout << " " << m_Name << "list updated" << std::endl;	
	}

	void queryDialog() override
	{
		std::cout << " " << m_Name << "list queried" << std::endl;
	}
};

class Button : public Dialog
{
public:
	Button(Mediator * _mediator, std::string const & _name)
		: Dialog(_mediator, _name)
	{
	}

	void updateDialog() override
	{
		std::cout << " " << m_Name << "button updated" << std::endl;
	}

	void queryDialog() override
	{
		std::cout << " " << m_Name << "button queried" << std::endl;
	}
};


class Edit : public Dialog
{
public:
	Edit(Mediator * _mediator, std::string const & _name)
		: Dialog(_mediator, _name)
	{
	}

	void updateDialog() override
	{
		std::cout << " " << m_Name << "Edit updated" << std::endl;
	}

	void queryDialog() override
	{
		std::cout << " " << m_Name << "Edit queried" << std::endl;
	}
};

class Mediator
{
public:
	enum Dialogs {StartButton, EditName, FileList, QuitButton, ResultList, Amount};

	Mediator()
	{
		m_Components[StartButton] = new Button(this, "Start");
		m_Components[QuitButton] = new Button(this, "Quit");
		m_Components[EditName] = new Edit(this, "Edit name");
		m_Components[ResultList] = new List(this, "Result List");
		m_Components[FileList] = new List(this, "File List");
	}

	~Mediator()
	{
		for (int i{}; i < Dialogs::Amount; i++)
			delete m_Components[i];
	}

	void handleEvent(int _num)
	{
		assert(_num < Dialogs::Amount);
		m_Components[_num]->changed();
	}

	void DialogChanged(Dialog * _dialog)
	{
		if (_dialog == m_Components[Dialogs::StartButton])
		{
			m_Components[Dialogs::StartButton]->queryDialog();
			m_Components[Dialogs::EditName]->updateDialog();
		}
		else if (_dialog == m_Components[Dialogs::EditName])
		{
			m_Components[Dialogs::EditName]->queryDialog();
			m_Components[Dialogs::FileList]->updateDialog();
		}
		else if (_dialog == m_Components[Dialogs::QuitButton])
		{
			m_Components[Dialogs::QuitButton]->queryDialog();
			m_Components[Dialogs::ResultList]->updateDialog();
			m_Components[Dialogs::FileList]->updateDialog();
		}
		else if (_dialog == m_Components[Dialogs::FileList])
		{
			m_Components[Dialogs::FileList]->updateDialog();
		}
		else if (_dialog == m_Components[Dialogs::ResultList])
		{
			m_Components[Dialogs::ResultList]->updateDialog();
			m_Components[Dialogs::FileList]->queryDialog();
		}

	}

private:
	Dialog * m_Components[Dialogs::Amount];
};

void Dialog::changed()
{
	m_Mediator->DialogChanged(this);
}

int main()
{
	Mediator m;
	m.handleEvent(Mediator::Dialogs::QuitButton);

	return 0;
}