#include <iostream>
#include <list>
#include <string>

class Listener
{
	std::string Email;
public:
	Listener(std::string const & _email)
		:Email(_email) 
	{}
	std::string const & getEmail() const
	{
		return Email;
	}
	virtual void notify(class Publisher *) = 0;
};

class Publisher
{
	std::string m_CurrentPub;
	std::list<Listener* > m_Listeners;
public:
	Publisher() = default;

	~Publisher()
	{
		for (auto it : m_Listeners)
			delete it;
	}

	void subscribe(Listener * _lis)
	{
		m_Listeners.push_back(_lis);
	}

	void unsubscribe(Listener * _lis)
	{
		m_Listeners.remove(_lis);
	}

	void nitifyAll()
	{
		for (auto & it : m_Listeners)
			it->notify(this);
	}

	std::string const & getPublication() const
	{
		return m_CurrentPub;
	}

	void setCurrentPublication(std::string const & _pub)
	{
		m_CurrentPub = _pub;
		nitifyAll();
	}
};

class SimpleListeners : public Listener
{
public:
	SimpleListeners(std::string const & _email)
		:Listener(_email)
	{}
	void notify(Publisher * _pub) override
	{
		std::cout << "Notifeing simple listener " << getEmail() << " about " << _pub->getPublication() << std::endl;
	}
};

class EliteListeners : public Listener
{
public:
	EliteListeners(std::string const & _email)
		:Listener(_email)
	{}
	void notify(Publisher * _pub) override
	{
		std::cout << "Notifeing elite listener " << getEmail() << " about " << _pub->getPublication() << std::endl;
	}
};

int main()
{
	Publisher p;
	EliteListeners * another = new EliteListeners("haha@gmail.com");
	SimpleListeners * mirash = new SimpleListeners("Mirash@mail.ru");
	p.subscribe(new SimpleListeners("AZad@gmail.com"));
	p.subscribe(new EliteListeners("Elitka@gmail.com"));
	p.subscribe(mirash);
	p.subscribe(new SimpleListeners("kek@mail.ru"));

	p.setCurrentPublication("World Cup 2018");

	p.unsubscribe(another);
	p.unsubscribe(mirash);

	std::cout << std::endl;
	p.setCurrentPublication("cpp17 for C++");

	delete another;
	return 0;
}