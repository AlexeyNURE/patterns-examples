#include "Network.hpp"
#include "Iterator.hpp"

ProfileIterator * Facebook::createFriendsIterator(int _id)
{
	return new FacebookIterator(*this, _id, "Friend");
}

ProfileIterator * Facebook::createCoworkersIterator(int _id)
{
	return new FacebookIterator(*this, _id, "Coworker");
}

