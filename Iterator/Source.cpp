#include <iostream>
#include "Iterator.hpp"
#include "Network.hpp"


class SocialSpamer
{
public:
	void send(ProfileIterator & _it, std::string const & _message)
	{
		while (_it.hasMore())
			_it.getNext().sendMessage(_message);
	}
};

class App
{
	SocialNetwork * m_network;
	SocialSpamer * m_spamer;
public:
	App(Facebook & _facebook,SocialSpamer & _spamer )
		: m_network(&_facebook), m_spamer(&_spamer)
	{
	}

	void sendSpamToFriends(Profile & _pr)
	{
		auto it = m_network->createFriendsIterator(_pr.id);
		m_spamer->send(*it, "some message for friends");
		delete it;
	}

	void sendSpamToCoworkers(Profile & _pr)
	{
		auto it = m_network->createCoworkersIterator(_pr.id);
		m_spamer->send(*it, "some message for coworkers");
		delete it;
	}
};

int main()
{
	Facebook facebook;
	Profile one("Alexey", 123, "Friend");

	facebook.m_Profiles.push_back(one);
	facebook.m_Profiles.push_back(Profile("Kolya", 124, "Friend"));
	facebook.m_Profiles.push_back(Profile("Sasha", 125, "Coworker"));
	facebook.m_Profiles.push_back(Profile("Vasya", 126, "Coworker"));

	SocialSpamer spamer;

	App app(facebook, spamer);

	app.sendSpamToFriends(one);



	return 0;
}

