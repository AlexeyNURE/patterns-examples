#pragma once
#include <iostream>
#include <vector>
#include <string>

class ProfileIterator;

struct Profile
{
	int id;
	std::string name;
	std::string type;

	Profile(std::string const & _name, int _id, std::string const & _type)
		: name(_name), id(_id), type(_type)
	{}

	void sendMessage(std::string const & _message)
	{
		std::cout << "Sending message to " << name << " : " << _message << std::endl;
	}
};

struct SocialNetwork
{
	virtual ProfileIterator * createFriendsIterator(int _id) = 0;
	virtual ProfileIterator * createCoworkersIterator(int _id) = 0;
};

struct Facebook : public SocialNetwork
{
	std::vector<Profile> m_Profiles;
public:
	ProfileIterator * createFriendsIterator(int _id) override;
	ProfileIterator * createCoworkersIterator(int _id) override;
	std::vector<Profile> getPeople(int _id, std::string const & _type)
	{
		std::vector<Profile> res;
		for (auto it : m_Profiles)
			if (it.type == _type)
				res.push_back(it);

		return res;
	}
};