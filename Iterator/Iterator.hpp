#pragma once
#include <iostream>
#include <vector>
#include "Network.hpp"

class ProfileIterator
{
public:
	ProfileIterator() = default;
	virtual Profile & getNext() = 0;
	virtual bool hasMore() = 0;
};

class FacebookIterator : public ProfileIterator
{
	Facebook & m_Facebook;
	int m_ID;
	std::string m_Type;
	int m_CurrentPosition;
	std::vector<Profile> m_Cache;

	void lazyInit()
	{
		if (m_Cache.empty())
			m_Cache = m_Facebook.getPeople(m_ID, m_Type);
	}
public:
	FacebookIterator(Facebook & _facebook, int _id, std::string const & _type)
		: ProfileIterator(), m_Facebook(_facebook), m_ID(_id), m_Type(_type)
	{
	}

	~FacebookIterator() = default;

	Profile & getNext() override
	{
		if (hasMore())
			m_CurrentPosition++;
		return m_Cache[m_CurrentPosition - 1];
	}

	bool hasMore() override
	{
		lazyInit();
		return m_Cache.size() > m_CurrentPosition;
	}
};