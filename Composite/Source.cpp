#include <iostream>
#include <cassert>
#include <vector>

class Unit
{
	int m_Strength;
	int m_HP;
public:
	Unit(int _strenght, int _hp)
		: m_Strength(_strenght), m_HP(_hp)
	{}

	virtual int getStrenght() const
	{
		return m_Strength;
	}

	virtual int getHP() const
	{
		return m_HP;
	}

	virtual void addUnit(Unit * _unit)
	{
		assert(false);
	}

	virtual ~Unit() = default;
};

class Archer : public Unit
{
public:
	Archer()
		: Unit(3, 10)
	{
	}
};

class Horseman : public Unit
{
public: 
	Horseman()
		: Unit(5, 15)
	{}
};

class Dragon : public Unit
{
public:
	Dragon()
		: Unit(10, 20)
	{}
};

// whole army
class CompositeUnit : public Unit
{
	std::vector<Unit *> m_Units;
public:
	CompositeUnit()
		: Unit(0, 0)
	{}

	~CompositeUnit()
	{
		for (auto it : m_Units)
			delete it;
	}

	int getStrenght() const override
	{
		int total = 0;
		for (auto it : m_Units)
			total += it->getStrenght();

		return total;
	}

	int getHP() const override
	{
		int total = 0;
		for (auto it : m_Units)
			total += it->getHP();

		return total;
	}

	void addUnit(Unit * _unit) override
	{
		m_Units.push_back(_unit);
	}
};

// creating legion
CompositeUnit * createLegion()
{
	CompositeUnit * legion = new CompositeUnit;

	// 10 archers
	for (int i(0); i < 10; i++)
		legion->addUnit(new Archer);

	// 15 Horsemen
	for (int i{}; i < 15; i++)
		legion->addUnit(new Horseman);

	// 5 dragons
	for (int i = 0; i < 5; i++)
		legion->addUnit(new Dragon);

	return legion;
}

int main()
{
	CompositeUnit * Army = new CompositeUnit;

	// army icnludes four legions
	Army->addUnit(createLegion());
	Army->addUnit(createLegion());
	Army->addUnit(createLegion());
	Army->addUnit(createLegion());

	std::cout << "The strenght of the whole army is " << Army->getStrenght() << std::endl;
	std::cout << "The hp of the whole army is " << Army->getHP() << std::endl;

	delete Army;
	return 0;
}