#include <iostream>
#include <vector>
#include <Windows.h>
#include <cassert>

struct Route
{
	std::vector<POINT> m_Points;
	int m_Price;
	int m_Time;
};

class RouteStrategy
{
public:
	virtual Route buildRoute(POINT, POINT) = 0;
};

class RoadStrategy : public RouteStrategy
{
public:
	Route buildRoute(POINT A, POINT B) override
	{
		Route res;
		res.m_Points.push_back(POINT{3, 3}); // some array of points that represent route
		res.m_Points.push_back(POINT{ 3, 4 });
		res.m_Points.push_back(POINT{ 3, 5 });
		res.m_Points.push_back(POINT{ 3, 6 });
		res.m_Price = 30;
		res.m_Time = 5;
		return res;
	}
};

class WalkingStrategy : public RouteStrategy
{
public:
	Route buildRoute(POINT A, POINT B) override
	{
		Route res;
		res.m_Points.push_back(POINT{ 3, 3 }); // some array of points that represent route
		res.m_Points.push_back(POINT{ 3, 4 });
		res.m_Points.push_back(POINT{ 3, 5 });
		res.m_Points.push_back(POINT{ 3, 6 });
		res.m_Price = 0;
		res.m_Time = 30;
		return res;
	}
};	

class PublicTransportStrategy : public RouteStrategy
{
public:
	Route buildRoute(POINT A, POINT B) override
	{
		Route res;
		res.m_Points.push_back(POINT{ 3, 3 }); // some array of points that represent route
		res.m_Points.push_back(POINT{ 3, 4 });
		res.m_Points.push_back(POINT{ 3, 5 });
		res.m_Points.push_back(POINT{ 3, 6 });
		res.m_Price = 15;
		res.m_Time = 15;
		return res;
	}
};

class Navigator
{
	RouteStrategy * m_Strategy;
	Route m_Route;
public:
	Navigator()
	{
		m_Strategy = nullptr;
	}

	~Navigator()
	{
		delete m_Strategy;
	}

	void setStrategy(RouteStrategy * _strategy)
	{
		delete m_Strategy;
		m_Strategy = _strategy;
	}

	void buildRoute(POINT A, POINT B)
	{
		m_Route = m_Strategy->buildRoute(A, B);
	}

	Route & getRoute()
	{
		return m_Route;
	}
};

int main()
{
	Navigator n;
	n.setStrategy(new PublicTransportStrategy);
	n.buildRoute({ 1, 3 }, {3, 5});
	
	assert(n.getRoute().m_Price == 15);
	assert(n.getRoute().m_Time == 15);
	return 0;
}