#include <iostream>
#include <vector>

enum class Problems {Info, Bonuses, Balance, Operator};

class AbstractSolution
{
public:
	virtual void help(Problems _problem) = 0;
};

class Solution
	: public AbstractSolution
{
protected:
	Problems m_Problem;
	Solution * m_NextSolution;
public:
	Solution()
	{
		m_NextSolution = nullptr;
	}

	void setNext(Solution * _sol)
	{
		m_NextSolution = _sol;
	}

	void add(Solution * _sol)
	{
		if (m_NextSolution)
		{
			m_NextSolution->add(_sol);
		}
		else setNext(_sol);
	}

	void setProblem(Problems _problem)
	{
		m_Problem = _problem;
	}

	Problems getProblem() const
	{
		return m_Problem;
	}

	~Solution()
	{
	}

	void help(Problems _problem) override
	{
		if (m_NextSolution)
			m_NextSolution->help(_problem);
	}
};


class PressOne 
	: public Solution
{
public:
	virtual void help(Problems _problem) override
	{
		if (_problem == Problems::Info)
			std::cout << "Press one to know your number info\n";
		else Solution::help(_problem);
	}
};

class PressTwo
	: public Solution
{
public:
	virtual void help(Problems _problem) override
	{
		if (_problem == Problems::Bonuses)
			std::cout << "Press two to listen to your bonuses\n";
		else Solution::help(_problem);
	}
};

class PressThree
	: public Solution
{
public:
	virtual void help(Problems _problem) override
	{
		if (_problem == Problems::Balance)
			std::cout << "Press three to know your balance\n";
		else Solution::help(_problem);
	}
};


class Operator : public Solution
{
public:
	void help(Problems _problem) override
	{
		if (_problem == Problems::Operator)
			std::cout << "Trying to connect to the  operator ...\n";
		else Solution::help(_problem);
	}
};

class App
{
	Solution * m_Solution;
	std::vector<Solution *> m_SolutionArray;
public:
	App()
	{
		m_Solution = new PressOne;
		PressTwo * two = new PressTwo;
		PressThree * three = new PressThree;
		Operator * op = new Operator;

		m_Solution->add(two);
		m_Solution->add(three);
		m_Solution->add(op);
		op->setNext(m_Solution);

		m_SolutionArray.push_back(m_Solution);
		m_SolutionArray.push_back(two);
		m_SolutionArray.push_back(three);
		m_SolutionArray.push_back(op);
	}

	~App()
	{
		for (auto it : m_SolutionArray)
			delete it;
	}

	void call(Problems _problem)
	{
		m_Solution->help(_problem);
	}
};

int main()
{
	App app;
	app.call(Problems::Operator);
	app.call(Problems::Balance);

	return 0;
}