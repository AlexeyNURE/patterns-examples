#pragma once
#include <iostream>
#include <vector>
#include "Command.hpp"

struct App
{
	std::string clipboard;
	std::vector<Editor> editors;
	Editor * activeEditor;
	CommandHistory History;
public:
	App() = default;

	void executeCommand(Command * _command)
	{
		if (_command->execute())
			History.push(_command);
	}

	void undo()
	{
		Command * command = &History.top();
		command->undo();
	}
};