#pragma once
#include <iostream>

class Editor
{
	std::string text;
public:
	std::string const & getSelection() const
	{
		return text;
	}

	void deleteSelection()
	{
		text.clear();
	}

	void replaceSelection(std::string const & _text)
	{
		text = _text;
	}
};
