#include <iostream>
#include <vector>
#include "app.hpp"
#include "Command.hpp"


int main()
{

	// somewhere in client code...
	/*
	we can use commands in different contexts, for exhample when button is pressed or when shortcut
	*/
	App appOne;
	Editor editor;
	Editor editor2;
	editor.replaceSelection("some text");
	CopyCommand copy(&appOne, &editor);
	CutCommand cut(&appOne, &editor);
	PasteCommand paste(&appOne, &editor2);
	appOne.executeCommand(&copy);
	appOne.executeCommand(&paste);
	assert(editor2.getSelection() == "some text");
	appOne.executeCommand(&cut);
	assert(editor.getSelection() == "");
	assert(editor2.getSelection() == "some text");
	appOne.undo();
	assert(editor.getSelection() == "some text");
	assert(editor2.getSelection() == "some text");
	return 0;
}