#include "app.hpp"
#include "Command.hpp"


Command::Command(App * _app, Editor * _editor)
	: app(_app), editor(_editor)
{
}

void Command::saveBackup()
{
	backup = editor->getSelection();
}

void Command::undo()
{
	editor->replaceSelection(backup);
}


CommandHistory::~CommandHistory()
{
}

void CommandHistory::push(Command * _command)
{
	m_History.push(_command);
}

void CommandHistory::pop()
{
	m_History.pop();
}

Command & CommandHistory::top() const
{
	assert(!m_History.empty());
	return *m_History.top();
}



CopyCommand::CopyCommand(App * _app, Editor * _editor)
	: Command(_app, _editor)
{}

bool CopyCommand::execute() 
{
	app->clipboard = editor->getSelection();
	return false;
}



CutCommand::CutCommand(App * _app, Editor * _editor)
	: Command(_app, _editor)
{}


bool CutCommand::execute() 
{
	saveBackup();
	app->clipboard = editor->getSelection();
	editor->deleteSelection();
	return true;
}



PasteCommand::PasteCommand(App * _app, Editor * _editor)
	: Command(_app, _editor)
{}
bool PasteCommand::execute()
{
	saveBackup();
	editor->replaceSelection(app->clipboard);
	return true;
}



UndoCommand::UndoCommand(App * _app, Editor * _editor)
	: Command(_app, _editor)
{}


bool UndoCommand::execute()
{
	app->undo();
	return true;
}

