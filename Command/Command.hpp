#pragma once
#include <stack>
#include <cassert>
#include "editor.hpp"

class App;

class Command
{
protected:
	App * app;
	Editor * editor;
	std::string backup;

public:
	Command(App * _app, Editor * _editor);

	void saveBackup();
	void undo();
	virtual bool execute() = 0; // if is true -- save command in history
};

class CommandHistory
{
	std::stack<Command *> m_History;
public:
	CommandHistory() = default;

	~CommandHistory();

	void push(Command * _command);

	void pop();

	Command & top() const;
};


class CopyCommand
	: public Command
{
public:
	CopyCommand(App * _app, Editor * _editor);

	bool execute() override;
};

class CutCommand
	: public Command
{
public:
	CutCommand(App * _app, Editor * _editor);

	bool execute() override;
};

class PasteCommand
	: public Command
{
public:
	PasteCommand(App * _app, Editor * _editor);

	bool execute() override;
};

class UndoCommand : public Command
{
public:
	UndoCommand(App * _app, Editor * _editor);

	bool execute() override;
};
