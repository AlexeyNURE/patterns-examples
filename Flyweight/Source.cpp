#include <iostream>
#include <vector>
#include <set>

using color = std::string;
using texture = std::string;

// flyweight
struct TreeType
{
	std::string m_Name;
	color m_Color;
	texture m_Texture;

public:
	TreeType(std::string const & _name, color const & _color, texture const & _texture)
		: m_Name(_name), m_Color(_color), m_Texture(_texture)
	{}

	TreeType(const TreeType &) = delete;

	TreeType operator = (const TreeType &) = delete;

	bool operator == (const TreeType & _type) const
	{
		return m_Name == _type.m_Name && m_Color == _type.m_Color && m_Texture == _type.m_Texture;
	}

	bool operator != (const TreeType & _type) const
	{
		return !(*this == _type);
	}

	~TreeType() = default;

	void draw(int _x, int _y) const
	{
		std::cout << "Drawing tree " << m_Name.c_str() << " in pos " << _x << ":" << _y;
		std::cout << m_Color.c_str() << " " << m_Texture.c_str() << std::endl;
	}
};

class Tree
{
	const TreeType * m_Type;
	int m_x;
	int m_y;
public:
	Tree(int _x, int _y, TreeType * _type)
		:m_x(_x), m_y(_y), m_Type(_type)
	{}

	~Tree() = default;

	void draw() const
	{
		m_Type->draw(m_x, m_y);
	}
};

class TreeFactory
{
	std::set<TreeType *> m_Type;

public:
	TreeFactory()
	{
	}

	TreeType * getTreeType(std::string const & _name, color const & _color, texture const & _texture)
	{
		TreeType * type = nullptr;
		for (auto const & it : m_Type)
			if (it->m_Name == _name && it->m_Color == _color && it->m_Texture == _texture)
				type = it;

		if (!type)
		{
			type = new TreeType(_name, _color, _texture);
			m_Type.insert(type);
		}

		return type;
	}

	~TreeFactory()
	{
		for (auto it : m_Type)
			delete it;
	}
};

class Forest
{
	std::vector<Tree *> m_Trees;
	TreeFactory * m_Factory;
public:
	Forest()
	{
		m_Factory = new TreeFactory;
	}

	~Forest()
	{
		for (auto it : m_Trees)
			delete it;

		delete m_Factory;
	}

	void plantTree(int _x, int _y, std::string const & _name, color const & _color, texture const & _texture)
	{
		TreeType * type = m_Factory->getTreeType(_name, _color, _texture);
		Tree * tree = new Tree(_x, _y, type);
		m_Trees.push_back(std::move(tree));
	}

	void draw() const
	{
		for (auto const & it : m_Trees)
			it->draw();
	}
};

int main()
{
	Forest forest;
	forest.plantTree(3, 3, "Sosna", "green", "texture_1");
	forest.plantTree(5, 4, "Sosna", "green", "texture_1");
	forest.plantTree(7, 10, "El", "blue", "texture_2");
	forest.plantTree(6, 6, "Sosna", "green", "texture_1");
	forest.plantTree(15, 10, "El", "blue", "texture_2");


	return 0;
}