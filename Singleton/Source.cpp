#include <iostream>
#include <cassert>

class Singleton
{
	static Singleton * m_Inst;
	int value;
	Singleton() = default;
public:
	void set(int _val)
	{
		value = _val;
	}
	int getValue() const
	{
		return value;
	}
	static Singleton * getInstance()
	{
		if (!m_Inst)
			m_Inst = new Singleton;

		return m_Inst;
	}
};

Singleton *Singleton::m_Inst = nullptr;

int main()
{
	{
		auto inst = Singleton::getInstance();
		inst->set(3);
		assert(inst->getValue() == 3);
	}
	
	assert(Singleton::getInstance()->getValue() == 3);

	Singleton::getInstance()->set(5);

	Singleton * inst = Singleton::getInstance();
	assert(inst->getValue() == 5);


	delete inst;
	return 0;
}