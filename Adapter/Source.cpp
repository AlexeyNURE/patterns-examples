#include <iostream>
#include <string>

using Cable = int;

struct EuropeanSocketInterface
{
	virtual int voltage() = 0;
	virtual Cable live() = 0;
	virtual Cable neutral() = 0;
	virtual Cable earth() = 0;
};

struct Socket : public EuropeanSocketInterface
{
	int voltage() override
	{
		return 220;
	}

	virtual Cable live() override
	{
		return 1;
	}

	virtual Cable neutral() override
	{
		return -1;
	}

	virtual Cable earth() override
	{
		return 0;
	}
};

struct USASocketInterface
{
	virtual int voltage() = 0;

	virtual Cable live() = 0;
	virtual Cable neutral() = 0;
};

//EuaropeanSocket to USASocket Adapter
struct Adapter : public USASocketInterface
{
private:
	EuropeanSocketInterface * m_Socket;
public:
	void plugIn(EuropeanSocketInterface * _socket)
	{
		m_Socket = _socket;
	}

	int voltage()
	{
		return 110;
	}

	Cable live() override
	{
		return m_Socket->live();
	}

	Cable neutral() override
	{
		return m_Socket->neutral();
	}
};

//EuaropeanSocket to USASocket Adapter
struct Euro_to_USA_Adapter : public USASocketInterface, public Socket
{
public:

	int voltage()
	{
		return 110;
	}

	Cable live() override
	{
		return Socket::live();
	}

	Cable neutral() override
	{
		return Socket::neutral();
	}

	Cable earth() override
	{
		return Socket::earth();
	}
};

//Client
class ElectricKettle
{
	USASocketInterface * m_Power;

public:
	void plugIn(USASocketInterface * _power)
	{
		m_Power = _power;
	}

	void boil()
	{
		if (m_Power->voltage() > 110)
		{
			std::cout << "Kettle is on power\n";
			return;
		}

		if (m_Power->live() == 1 && m_Power->neutral() == -1)
			std::cout << "Coffee is ready\n";
	}
};

int main()
{
	// #1
	Socket * socket = new Socket;
	Adapter * adapter = new Adapter;
	ElectricKettle * kettle = new ElectricKettle;

	adapter->plugIn(socket);
	kettle->plugIn(adapter);

	kettle->boil();

	delete socket;
	delete adapter;
	delete kettle;
	
	// #2
	Euro_to_USA_Adapter * adapter2 = new Euro_to_USA_Adapter;
	ElectricKettle * kettle2 = new ElectricKettle;

	kettle2->plugIn(adapter2);
	kettle2->boil();

	delete adapter2;
	delete kettle2;
	return 0;
}