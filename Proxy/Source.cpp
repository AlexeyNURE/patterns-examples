#include <iostream>
#include <memory>
#include <vector>
#include <string>

class YouTubeLib
{
public:
	virtual std::vector<int> getVideoList() = 0;
	virtual std::string getVideoInfo(int _id) = 0;
	virtual void downloadVideo(int _id) = 0;
};

class YouTubeAPI : public YouTubeLib
{
public:
	std::vector<int> getVideoList() override
	{
		// long operation using YouTube API
		std::vector<int > res;
		res.push_back(123);
		res.push_back(1234);
		res.push_back(12345);
		return res;
	}
	std::string getVideoInfo(int _id) override
	{
		// getting info from server
		std::string res("some info about ");
		res += std::to_string(_id);

		std::cout << res.c_str();
		return res;
	}
	void downloadVideo(int _id) override
	{
		std::cout << "downloading video " << _id << std::endl;
		// downloading video from youtube....
	}
};

class YouTubeCachedProxy
	: public YouTubeLib
{
	std::shared_ptr<YouTubeAPI> m_YouTube;  // ref counting
	std::vector<int> listCache;
	std::string videoCache;
	bool needReset;
public:
	YouTubeCachedProxy(YouTubeAPI * _api)
	{
		m_YouTube.reset(std::move(_api));
	}

	std::vector<int> getVideoList() override
	{
		if (listCache.empty() || needReset)
			listCache = m_YouTube->getVideoList();

		return listCache;
	}

	std::string getVideoInfo(int _id) override
	{
		if (videoCache.empty() || needReset)
			videoCache = m_YouTube->getVideoInfo(_id);

		return videoCache;
	}

	void downloadVideo(int _id) override
	{
		if (!needReset)
			m_YouTube->downloadVideo(_id);
	}
};

class YouTubeManager
{
	YouTubeLib * m_YouTubeLib;
	std::vector<int> listCache;
	std::string videoCache;

public:
	YouTubeManager(YouTubeLib * _lib)
		: m_YouTubeLib(_lib)
	{}

	~YouTubeManager()
	{
		delete m_YouTubeLib;
	}

	void renderVideo(int _id)
	{
		videoCache = m_YouTubeLib->getVideoInfo(_id);
	}

	void renderVideoPanel()
	{
		listCache = m_YouTubeLib->getVideoList();
	}
};

int main()
{	
	YouTubeAPI * api = new YouTubeAPI;
	YouTubeCachedProxy * proxy = new YouTubeCachedProxy(api);

	YouTubeManager * manager = new YouTubeManager(proxy);

	manager->renderVideo(123);

	delete manager;
	delete api;

	return 0;
}