#include "MP3Player.hpp"
#include "State.hpp"

MP3Player::MP3Player()
{
	m_State = new LockedState(*this);
}

MP3Player::~MP3Player()
{
	delete m_State;
}

void MP3Player::setState(State * _state)
{
	delete m_State;
	m_State = _state;
}

void MP3Player::clickLock()
{
	m_State->clickLock();
}

void MP3Player::clickNext()
{
	m_State->clickNext();
}

void MP3Player::clickPlay()
{
	m_State->clickPlay();
}

void MP3Player::clickPrev()
{
	m_State->clickPrev();
}

bool MP3Player::isPlaying() const
{
	return Playing;
}

void MP3Player::startPlayback()
{
	Playing = true;
}

void MP3Player::stopPlayback()
{
	Playing = false;
}

void MP3Player::nextSong()
{
	std::cout << "Playing next song\n";
}

void MP3Player::PrevSong()
{
	std::cout << "Playing previous song\n";
}

