#pragma once
#include <iostream>
#include <string>

class MP3Player;

class State
{
protected:
	MP3Player & m_Player;

public:
	State(MP3Player & _player);

	virtual ~State() = default;

	virtual void clickLock() = 0;

	virtual void clickPlay() = 0;

	virtual void clickNext() = 0;

	virtual void clickPrev() = 0;
};

class LockedState
	: public State
{
public:
	LockedState(MP3Player & _player);

	void clickLock() override;

	void clickPlay() override;

	void clickNext() override;

	void clickPrev() override;

};

class ReadyState
	: public State
{
public:
	ReadyState(MP3Player & _player);

	void clickLock() override;

	void clickPlay() override;

	void clickNext() override;

	void clickPrev() override;

};

class PlayingState
	: public State
{
public:
	PlayingState(MP3Player & _player);

	void clickLock() override;

	void clickPlay() override;

	void clickNext() override;

	void clickPrev() override;
};
