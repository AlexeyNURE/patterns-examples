#pragma once
#include <iostream>
#include <vector>

class State;

class MP3Player
{
	State * m_State;
	int m_Volume;
	bool Playing;
	std::vector<std::string> m_Playlist;
	std::string m_CurrentSong;

public:
	MP3Player();

	~MP3Player();

	void setState(State * _state);

	bool isPlaying() const;

	void clickLock();

	void clickPlay();

	void clickNext();

	void clickPrev();

	void startPlayback();

	void stopPlayback();

	void nextSong();

	void PrevSong();
};