#include "MP3Player.hpp"
#include "State.hpp"
#include <cassert>

int main()
{
	MP3Player player;  // locked
	player.clickLock(); // ready
	player.clickPlay(); // playing
	assert(player.isPlaying());

	player.clickPlay(); // ready
	assert(!player.isPlaying());
	player.clickLock(); // locked

	player.clickPlay(); // locked
	assert(!player.isPlaying());

	return 0;
}