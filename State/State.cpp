#include "MP3Player.hpp"
#include "State.hpp"

State::State(MP3Player & _player)
	: m_Player(_player)
{}

LockedState::LockedState(MP3Player & _player)
	: State(_player)
{
}

void LockedState::clickLock()
{
	if (m_Player.isPlaying())
		m_Player.setState(new PlayingState(m_Player));
	else m_Player.setState(new ReadyState(m_Player));
}

void LockedState::clickNext()
{
	// do nothing
}

void LockedState::clickPrev()
{
	// do nothing
}

void LockedState::clickPlay()
{
	// do nothing
}

ReadyState::ReadyState(MP3Player & _player)
	:State(_player)
{
}

void ReadyState::clickLock()
{
	m_Player.setState(new LockedState(m_Player));
}

void ReadyState::clickNext()
{
	m_Player.nextSong();
}

void ReadyState::clickPrev()
{
	m_Player.PrevSong();
}

void ReadyState::clickPlay()
{
	m_Player.setState(new PlayingState(m_Player));
}

PlayingState::PlayingState(MP3Player & _player)
	:State(_player)
{
	m_Player.startPlayback();
}

void PlayingState::clickLock()
{
	m_Player.setState(new LockedState(m_Player));
}

void PlayingState::clickPlay()
{
	m_Player.stopPlayback();
	m_Player.setState(new ReadyState(m_Player));
}

void PlayingState::clickNext()
{
	m_Player.nextSong();
}

void PlayingState::clickPrev()
{
	m_Player.PrevSong();
}