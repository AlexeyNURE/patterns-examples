#include <iostream>
#include <ctime>
#include <cassert>
#include <mutex>
#include <Windows.h>

// implementation
class Device
{
private:
	int m_Volume;
	bool Enabled;
	std::string m_Channel;
public:
	Device()
	{
		Enabled = false;
		m_Volume = 20;
	}
	bool isEnabled() const
	{
		return Enabled;
	}
	void enable()
	{
		Enabled = true;
	}
	void disable()
	{
		Enabled = false;
	}
	int getVolume() const
	{
		return m_Volume;
	}
	void setVolume(int _volume)
	{
		m_Volume = _volume;
	}
	std::string getChannel() const
	{
		return m_Channel;
	}
	void setChannel(std::string const & _chan)
	{
		m_Channel = _chan;
	}
};

// abstrction
class Remote
{
protected:
	Device * m_Device; // bridge
public:
	Remote(Device * _device)
	{
		m_Device = _device;
	}
	void power()
	{
		if (m_Device->isEnabled())
			m_Device->disable();
		else m_Device->enable();
	}
	void volumeUP()
	{
		m_Device->setVolume(m_Device->getVolume() + 10);
	}

	void volumeDown()
	{
		m_Device->setVolume(m_Device->getVolume() - 10);
	}

	void channelUP()
	{
		m_Device->setChannel("up");
	}

	void channelDown()
	{
		m_Device->setChannel("down");
	}
};

class AdvancedRemote : public Remote
{
	HANDLE Thread;
public:
	AdvancedRemote(Device * _dev)
		: Remote(_dev)
	{
	}
	void mute()
	{
		m_Device->setVolume(0);
	}

	void setTimer(int _minutes)
	{
		static clock_t current = clock();
		while (clock() - current <= _minutes);
			power();
	}
};

class TV : public Device
{
	
};

class Radio : public Device
{

};

int main()
{
	std::mutex m;
	TV * tv = new TV();
	AdvancedRemote * remote_for_tv = new AdvancedRemote(tv);
	remote_for_tv->power();

	Radio * radio = new Radio;
	Remote * remote = new Remote(radio);
	remote->power();

	assert(tv->isEnabled());
	assert(radio->isEnabled());

	remote_for_tv->mute();
	assert(tv->getVolume() == 0);

	remote_for_tv->setTimer(3000);

	assert(tv->isEnabled() == false);

	delete tv;
	delete remote_for_tv;

	delete radio;
	delete remote;

	return 0;
}