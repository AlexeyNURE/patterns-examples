#include <iostream>

class AbstractWeapon
{
public:
	virtual ~AbstractWeapon() = default;
	virtual void aim() {};
	virtual void fire() {};
};

class Weapon : public AbstractWeapon
{
public:
	void aim() override 
	{
		std::cout << "Weapon::aim()" << std::endl;
	}
	void fire() override 
	{
		std::cout << "Weapon::fire()" << std::endl;
	}
};


// decorator
class WeaponAccessory : public AbstractWeapon
{
	AbstractWeapon * m_Weapon;
public:
	WeaponAccessory(AbstractWeapon * _weapon)
		: m_Weapon(_weapon)
	{
	}
	virtual ~WeaponAccessory()
	{
		std::cout << "Decrator dtor\n";
		delete m_Weapon;
	}
	void aim() override
	{
		m_Weapon->aim();
	}
	void fire() override
	{
		m_Weapon->fire();
	}
};

// глушитель
class Silencer : public WeaponAccessory
{
public:
	Silencer(AbstractWeapon * _weapon)
		: WeaponAccessory(_weapon)
	{}

	void aim() override
	{
		std::cout << "Silencer ";
		WeaponAccessory::aim();
	}

	void fire() override
	{
		std::cout << "Silencer ";
		WeaponAccessory::fire();
	}
};

class PowerWeapon : public WeaponAccessory
{
public:
	PowerWeapon(AbstractWeapon * _weapon)
		: WeaponAccessory(_weapon)
	{}

	void aim() override
	{
		std::cout << "Power ";
		WeaponAccessory::aim();
	}

	void fire() override
	{
		std::cout << "Power ";
		WeaponAccessory::fire();
	}
};

int main()
{
	AbstractWeapon * weapon = new Weapon;
	weapon->aim();
	weapon->fire();

	weapon = new Silencer(weapon);
	weapon->aim();
	weapon->fire();

	weapon = new PowerWeapon(weapon);
	weapon->aim();
	weapon->fire();

	delete weapon;

	AbstractWeapon * w = new PowerWeapon(new Silencer(new Weapon));
	w->aim();
	w->fire();

	delete w;
	return 0;
}