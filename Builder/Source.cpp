#include <iostream>
#include <cassert>
enum class Meat { None, Beef, Pork, Veal, Turkey, Chicken };

class Pizza
{
	int size;
	bool Cheese;
	bool Pepperoni;
	bool Sausage;
	bool Tomato;
	bool Ketchup;
	bool Pineapple;
	bool Cucumber;
	Meat meat;

public:

	Pizza()
	{
		size = 0;
		Cheese = false;
		Pepperoni = false;
		Sausage = false;
		Tomato = false;
		Ketchup = false;
		Pineapple = false;
		Pineapple = false;
		Cucumber = false;
		meat = Meat::None;
	}

	void setSize(int _size)
	{
		size = _size;
	}

	void addCheese()
	{
		Cheese = true;
	}

	void addPepperoni()
	{
		Pepperoni = true;
	}

	void addSausage()
	{
		Sausage = true;
	}

	void addTomato()
	{
		Tomato = true;
	}

	void addKetchup()
	{
		Ketchup = true;
	}

	void addPineapple()
	{
		Pineapple = true;
	}

	void addCucumber()
	{
		Cucumber = true;
	}

	void addMeat(Meat _meat)
	{
		meat = _meat;
	}
};


struct Builder
{
	virtual ~Builder() = default;
	virtual void reset() {};
	virtual void setSize(int _size) = 0;
	virtual void addCheese() {}
	virtual void addPepperoni() {}
	virtual void addSausage() {};
	virtual void addTomato() {};
	virtual void addKetchup() {};
	virtual void addPineapple() {};
	virtual void addCucumber() {};
	virtual void addMeat(Meat _meat) {};
};

struct PizzaBuilder
	: public Builder
{
	Pizza * m_Pizza;

	~PizzaBuilder() = default;
	void reset() override
	{
		m_Pizza = new Pizza();
	}
	void setSize(int _size) override
	{
		m_Pizza->setSize(_size);
	}
	void addCheese() override
	{
		m_Pizza->addCheese();
	}
	void addPepperoni() override
	{
		m_Pizza->addPepperoni();
	}
	void addSausage() override
	{
		m_Pizza->addSausage();
	}
	void addTomato() override
	{
		m_Pizza->addTomato();
	}
	void addKetchup() override
	{
		m_Pizza->addKetchup();
	}
	void addPineapple() override
	{
		m_Pizza->addPineapple();
	}
	void addCucumber() override
	{
		m_Pizza->addCucumber();
	}
	void addMeat(Meat _meat) override
	{
		m_Pizza->addMeat(_meat);
	}

	Pizza * getResult() const
	{
		return m_Pizza;
	}
};

struct PizzaPriceBuilder
	: public Builder
{
	int m_PriceAmount;
	int m_Size;

	~PizzaPriceBuilder() = default;
	void reset() override
	{
		m_PriceAmount = 0;
		m_Size = 0;
	}
	void setSize(int _size) override
	{
		m_Size = _size;
	}
	void addCheese() override
	{
		m_PriceAmount += 10;
	}
	void addPepperoni() override
	{
		m_PriceAmount += 3;
	}
	void addSausage() override
	{
		m_PriceAmount += 12;
	}
	void addTomato() override
	{
		m_PriceAmount += 7;
	}
	void addKetchup() override
	{
		m_PriceAmount += 3;
	}
	void addPineapple() override
	{
		m_PriceAmount += 8;
	}
	void addCucumber() override
	{
		m_PriceAmount += 5;
	}
	void addMeat(Meat _meat) override
	{
		switch (_meat)
		{
		case Meat::Beef:
			m_PriceAmount += 15;
			break;
		case Meat::Chicken:
			m_PriceAmount += 12;
			break;
		case Meat::Pork:
			m_PriceAmount += 17;
			break;
		case Meat::Turkey:
			m_PriceAmount += 18;
			break;
		case Meat::Veal:
			m_PriceAmount += 16;
			break;
		default: 
			break;
		}
	}

	int getResult() const
	{
		int res = m_PriceAmount * m_Size;
		return res;
	}
};

struct Director
{
	void makeStandartPizza(Builder * _builder)
	{
		_builder->reset();
		_builder->addCheese();
		_builder->addKetchup();
		_builder->addTomato();
		_builder->addSausage();
		_builder->addCucumber();
		_builder->setSize(4);
	}

	void makeMeatPizza(Builder * _builder)
	{
		_builder->reset();
		_builder->addMeat(Meat::Chicken);
		_builder->addMeat(Meat::Beef);
		_builder->addMeat(Meat::Turkey);
		_builder->addKetchup();
		_builder->addTomato();
		_builder->addCucumber();
		_builder->setSize(4);
	}
};

class Client
{
	Pizza * m_Pizza;
	int price;
	Director * m_Director;

public:

	Client(Director * _dir)
	{
		m_Director = _dir;
	}
	void makeStandartPizza()
	{
		PizzaBuilder * builder = new PizzaBuilder();
		m_Director->makeStandartPizza(builder);
		m_Pizza = builder->getResult();
		PizzaPriceBuilder * priceBuilder = new PizzaPriceBuilder();
		m_Director->makeStandartPizza(priceBuilder);
		price = priceBuilder->getResult();

		delete builder;
		delete priceBuilder;
	}

	void makeMeatPizza()
	{
		PizzaBuilder * builder = new PizzaBuilder();
		m_Director->makeMeatPizza(builder);
		m_Pizza = builder->getResult();
		PizzaPriceBuilder * priceBuilder = new PizzaPriceBuilder();
		m_Director->makeMeatPizza(priceBuilder);
		price = priceBuilder->getResult();

		delete builder;
		delete priceBuilder;
	}

	int getPrice() const
	{
		return price;
	}

	~Client()
	{
		delete m_Pizza;
		delete m_Director;
	}
};

int main()
{
	Client Alexey(new Director());
	Alexey.makeStandartPizza();
	assert(Alexey.getPrice() == 148);


	PizzaBuilder builder;
	builder.reset();
	builder.addCheese();
	builder.addMeat(Meat::Pork);
	builder.addPineapple();
	builder.addKetchup();
	Pizza * pizza = builder.getResult();

	delete pizza;

	return 0;
}