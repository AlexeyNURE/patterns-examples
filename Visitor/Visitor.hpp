#pragma once
#include <iostream>

class Dot;
class Circle;
class Rectangle;
class CompoundShape;

class Visitor
{
public:
	virtual void visit(Dot &) = 0;
	virtual void visit(Circle &) = 0;
	virtual void visit(Rectangle &) = 0;
	virtual void visit(CompoundShape &) = 0;
};

class DefaultVisitor
	:	public Visitor
{
public:
	void visit(Dot &) {};
	void visit(Circle &) {};
	void visit(Rectangle &) {};
	void visit(CompoundShape &) {};
};

class XMLExportVisitor
	: public DefaultVisitor
{
public:
	void visit(Circle &) override;
	void visit(Rectangle &) override;
	void visit(CompoundShape &) override;
};
