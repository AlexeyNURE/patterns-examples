#include "Visitor.hpp"
#include "Shape.hpp"

void XMLExportVisitor::visit(Circle & _dot)
{
	std::cout << "Exporting Circle\n";
}

void XMLExportVisitor::visit(Rectangle & _dot)
{
	std::cout << "Exporting Rectangle\n";
}

void XMLExportVisitor::visit(CompoundShape & _dot)
{
	std::cout << "Exporting Compound Shape\n";
}