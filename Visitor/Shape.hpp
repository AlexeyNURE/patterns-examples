#pragma once
#include "Visitor.hpp"
#include <iostream>

class Shape
{
	int m_x;
	int m_y;
public:
	Shape(int _x, int _y)
		:m_x(_x), m_y(_y)
	{}

	virtual void draw() const { };

	virtual void accept(Visitor & _visitor) = 0;
};

class Dot : public Shape
{
public:
	Dot(int _x, int _y)
		: Shape(_x, _y)
	{}

	void draw() const
	{
		std::cout << "Dot\n";
	}

	void accept(Visitor & _v)
	{
		_v.visit(*this);
	}
};

class Circle : public Shape
{
	int m_Radius;
public:
	Circle(int _x, int _y, int _rad)
		: Shape(_x, _y), m_Radius(_rad)
	{}

	void draw() const
	{
		std::cout << "Circle\n";
	}

	void accept(Visitor & _v)
	{
		_v.visit(*this);
	}
};

class Rectangle : public Shape
{
public:
	Rectangle(int _x, int _y)
		: Shape(_x, _y)
	{}

	void draw() const
	{
		std::cout << "Rectangle\n";
	}

	void accept(Visitor & _v)
	{
		_v.visit(*this);
	}
};

class CompoundShape : public Shape
{
public:
	CompoundShape(int _x, int _y)
		: Shape(_x, _y)
	{}

	void draw() const
	{
		std::cout << "Compound Shape\n";
	}

	void accept(Visitor & _v)
	{
		_v.visit(*this);
	}
};