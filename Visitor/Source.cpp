#include <iostream>
#include "Visitor.hpp"
#include "Shape.hpp"
#include <vector>


class App
{
	std::vector<Shape *> m_Shapes;

public:
	App() = default;

	~App()
	{
		for (auto it : m_Shapes)
			delete it;
	}

	void add(Shape * _shape)
	{
		m_Shapes.push_back(_shape);
	}

	void drawAll() const
	{
		for (auto const & it : m_Shapes)
			it->draw();
	}

	void exportAll()
	{
		XMLExportVisitor v;
		for (auto & it : m_Shapes)
			it->accept( v );
	}
};

int main()
{
	App app;
	app.add(new Dot(3, 5));
	app.add(new Rectangle(3, 7));
	app.add(new CompoundShape(1, 1));
	app.add(new Rectangle(5, 5));
	app.add(new Circle(3, 6, 4));

	app.exportAll();

	app.drawAll();


	return 0;
}