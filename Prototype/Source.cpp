#include <iostream>
#include <vector>

class Shape
{
	int x;
	int y;
	std::string color;

public:
	Shape(int _x, int _y, std::string const & _color)
		:x(_x), y(_y), color(_color)
	{
	}

	virtual ~Shape() = default;

	Shape(const Shape & _shape)
	{
		x = _shape.x;
		y = _shape.y;
		color = _shape.color;
	}

	int getX() const
	{
		return x;
	}

	int getY() const
	{
		return y;
	}

	std::string const & getColor() const
	{
		return color;
	}

	virtual Shape * clone() = 0;
};

class Rectangle
	: public Shape
{
	int width;
	int height;

public:

	Rectangle(int _x, int _y, std::string const & _color, int _width, int _height)
		:Shape(_x, _y, _color), width(_width), height(_height)
	{
	}

	Rectangle(const Rectangle & _rect)
		:Shape(_rect)
	{
		width = _rect.width;
		height = _rect.height;
	}

	Shape * clone() override
	{
		return new Rectangle(*this);
	}

	int getWidth() const
	{
		return width;
	}

	int getHeight() const
	{
		return height;
	}
};

class Circle : public Shape
{
	int radius;

public:

	Circle(int _x, int _y, std::string const & _color, int _radius)
		: Shape(_x, _y, _color)
	{
		radius = _radius;
	}

	Circle(const Circle & _circle)
		: Shape(_circle)
	{
		radius = _circle.radius;
	}

	int getRadius() const
	{
		return radius;
	}

	Shape * clone() override
	{
		return new Circle(*this);
	}
};

class Application
{
	std::vector<Shape *> shapes;

public:
	Application()
	{
		Circle * circle = new Circle(2, 2, "black", 5);
		shapes.push_back(circle);

		Shape * anotherCircle = circle->clone();
		shapes.push_back(anotherCircle);

		Rectangle * rect = new Rectangle(3, 3, "white", 6, 6);
		shapes.push_back(rect);

		Shape * anotherRect = rect->clone();
		shapes.push_back(anotherRect);
	}

	~Application()
	{
		for (auto it : shapes)
			delete it;
	}

	std::vector<Shape*> copyVec()
	{
		// let's copy collection

		std::vector<Shape *> anotherVec;

		for (auto it : shapes)
			anotherVec.push_back(it->clone());

		return anotherVec;
	}
};

int main()
{
	Application app;
	auto vec = app.copyVec();

	for (auto it : vec)
		delete it;
}